import sys, requests, os, subprocess, platform
from PyQt5.QtCore import *
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QApplication, QLabel, QProgressBar, QPushButton
from PyQt5.QtWidgets import QLineEdit, QMessageBox
from PyQt5.QtWebEngineWidgets import QWebEngineView
from playsound import playsound

def getRetroMode():
    return '--retro-mode' in sys.argv

def getOS():
    return platform.system()

playStyleSheet = """
 QPushButton#playButton {
  background-image: url(resources/assets/PLAY1U.jpg);
  border: 0px;
  background-repeat: no-repeat;
 }
 QPushButton#playButton:pressed {
  background-image: url(resources/assets/btn-down.png);
  border: 0px;
 }
 QPushButton#playButton:hover {
  background-image: url(resources/assets/PLAY1R.jpg);
  border: 0px;
 }
"""

playStyleSheetRetro = """
 QPushButton#playButton {
  background-image: url(resources/retro/PLAY1U.jpg);
  border: 0px;
  background-repeat: no-repeat;
 }
 QPushButton#playButton:pressed {
  background-image: url(resources/retro/btn-down.png);
  border: 0px;
 }
 QPushButton#playButton:hover {
  background-image: url(resources/retro/PLAY1R.jpg);
  border: 0px;
 }
"""

class Globals:
    loginEndpoint = 'https://sunrise.games/api/login/alt/'
    serverType = 'Final Toontown'
    newsUrl = f'https://download.sunrise.games/launcher/getNews.php?serverType={serverType}'

    webHeaders = {
        'User-Agent': 'Duderino Games - Launcher'
    }

    noWindow = 0x08000000

class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowTitle('Toontown Online sv2.0 Launcher')
        self.setFixedWidth(750)
        self.setFixedHeight(500)

        icon = QIcon()
        icon.addPixmap(QPixmap('resources/assets/icon.png'), QIcon.Selected, QIcon.On)
        self.setWindowIcon(icon)

        label = QLabel(self)
        pixmap = QPixmap('resources/assets/BACKGROUND1.jpg' if not getRetroMode() else 'resources/retro/BACKGROUND1A.jpg')
        pixmap = pixmap.scaled(750, 500, Qt.KeepAspectRatio)
        label.setPixmap(pixmap)
        self.setCentralWidget(label)

        # Setup our GUI.
        self.setupGUI()

        self.resize(pixmap.width(), pixmap.height())
        self.center()
        self.oldPos = self.pos()

    def setupGUI(self):
        self.accountLabel = QLabel(self)
        self.accountLabel.setGeometry(QRect(420, 170, 101, 41))
        self.accountLabel.setPixmap(QPixmap('resources/assets/LIPROMPT.jpg' if not getRetroMode() else 'resources/retro/LIPROMPT.jpg'))
        self.accountLabel.setObjectName('accountLabel')

        self.usernameInput = QLineEdit(self)
        self.usernameInput.setGeometry(QRect(530, 170, 113, 20))
        self.usernameInput.setObjectName('usernameInput')

        self.passwordInput = QLineEdit(self)
        self.passwordInput.setGeometry(QRect(530, 200, 113, 20))
        self.passwordInput.setEchoMode(QLineEdit.Password)
        self.passwordInput.setObjectName('passwordInput')

        self.playButton = QPushButton(self)
        self.playButton.setStyleSheet(playStyleSheet if not getRetroMode() else playStyleSheetRetro)
        self.playButton.setGeometry(QRect(650, 180, 101, 51))
        self.playButton.setObjectName('playButton')

        # Bind the click event to our login method.
        self.playButton.clicked.connect(self.__handleLogin)

        self.news = QWebEngineView(self)
        self.news.setGeometry(QRect(30, 80, 301, 341))
        self.news.load(QUrl(Globals.newsUrl))
        self.news.setObjectName('news')

    def __handleLogin(self):
        # Play button was clicked, play our click sfx.
        playsound('resources/sfx/SNDCLICK.wav')

        username = self.usernameInput.text()
        password = self.passwordInput.text()

        data = {
            'username': username,
            'password': password,
            'serverType': Globals.serverType
        }

        request = requests.post(Globals.loginEndpoint, data = data, headers = Globals.webHeaders).json()

        if not request['success']:
            # Tell the user the reason why the login failed.
            self.setupPopup(request['message'])
        else:
            # Login was a success.
            # Prepare the environment variables.
            playToken = request['token']
            self.prepareVariables(playToken)

    def setupPopup(self, message):
        msg = QMessageBox()
        msg.setWindowTitle('Error')
        msg.setText(message)
        msg.exec_()

    def prepareVariables(self, playToken):
        os.environ['PLAY_TOKEN'] = playToken
        self.beginStart()
        self.exit()

    def beginStart(self):
        os.environ['WANT_SSL_SCHEME'] = 'True'
        os.environ['WANT_LOCALHOST_ASSETS'] = 'False'
        os.environ['GAME_SERVER'] = 'unite.sunrise.games:6667'
        os.chdir('sunrise')
        subprocess.Popen('py24 -m launcher', creationflags = Globals.noWindow)

    def exit(self):
        sys.exit()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def mousePressEvent(self, event):
        self.oldPos = event.globalPos()

    def mouseMoveEvent(self, event):
        delta = QPoint(event.globalPos() - self.oldPos)
        self.move(self.x() + delta.x(), self.y() + delta.y())
        self.oldPos = event.globalPos()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
